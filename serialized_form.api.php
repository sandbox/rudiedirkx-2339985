<?php

/**
 * Implements hook_serialized_form().
 */
function hook_serialized_form($instance, &$form_state, $values, $context) {
	if ($instance['field_name'] == 'field_my_field') {
		return array(
			'text' => array(
				'#type' => 'textfield',
				'#title' => t('Some textfield'),
				'#default_value' => @$values['text'],
			),
			'bool' => array(
				'#type' => 'checkbox',
				'#title' => t('Some checkbox'),
				'#default_value' => @$values['bool'],
			),
		);
	}
}
