<?php

/**
 * Implements hook_field_widget_info().
 */
function serialized_form_field_widget_info() {
  return array(
    'serialized_form' => array(
      'label' => t('Serialized form'),
      'field types' => array('text_long', 'blob', 'blob_long'),
      'settings' => array(),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
        'default value' => FIELD_BEHAVIOR_NONE,
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_settings_form().
 */
function serialized_form_field_widget_settings_form($field, $instance) {
  $settings = @$instance['widget']['settings']['serialized_form'] ?: array();

  return array(
    'serialized_form' => array(
      '#type' => 'fieldset',
      '#title' => t('Serialized form'),
      'collapsible' => array(
        '#type' => 'checkbox',
        '#title' => t('Collapsible?'),
        '#default_value' => isset($settings['collapsible']) ? $settings['collapsible'] : 1,
      ),
      'collapsed' => array(
        '#type' => 'checkbox',
        '#title' => t('Collapsed?'),
        '#default_value' => isset($settings['collapsed']) ? $settings['collapsed'] : 1,
      ),
      'clean' => array(
        '#type' => 'checkbox',
        '#title' => t('Clean up values?'),
        '#default_value' => isset($settings['clean']) ? $settings['clean'] : 1,
        '#description' => t('If enabled, simple empty values will be filtered from the form values before saving.'),
      ),
    ),
  );
}

/**
 * Helper to invoke all modules for the element/form without using module_invoke_all() to maintain pointers.
 */
function _serialized_form_invoke_all($instance, &$form_state, $values, $context) {
  $hook = 'serialized_form';
  $element = array();
  foreach (module_implements($hook) as $module) {
    $callback = $module . '_' . $hook;
    if (function_exists($callback)) {
      $result = $callback($instance, $form_state, $values, $context);
      if ($result && is_array($result)) {
        $element += $result;
      }
    }
  }

  return $element;
}

/**
 * Implements hook_field_widget_form().
 */
function serialized_form_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $values = array();
  if (isset($items[$delta]['value'])) {
    $serialized = $items[$delta]['value'];
    if ($serialized && ($unserialized = @unserialize($serialized))) {
      $values = $unserialized;
    }
  }

  // All context is useful, but only form_state MUST be by-reference.
  $context = compact('form', 'field', 'langcode', 'items', 'delta', 'element');
  $element += _serialized_form_invoke_all($instance, $form_state, $values, $context);
  $element['#tree'] = TRUE;

  $settings = $instance['widget']['settings']['serialized_form'];
  $element['#type'] = 'fieldset';
  $element['#collapsible'] = !empty($settings['collapsible']);
  $element['#collapsed'] = !empty($settings['collapsed']);

  unset($element['#value_callback']);
  $element['#element_validate'] = array('serialized_form_validate');

  return $element;
}

/**
 * Element validation & value callback for every serialized_form widget.
 */
function serialized_form_validate($element, &$form_state) {
  $values = drupal_array_get_nested_value($form_state['values'], $element['#parents']);

  if (!empty($element['#validate'])) {
    foreach ($element['#validate'] as $callback) {
      $callback($element, $values, $form_state);
    }
  }

  $instance = field_info_instance($element['#entity_type'], $element['#field_name'], $element['#bundle']);
  $settings = @$instance['widget']['settings']['serialized_form'] ?: array();

  $clean = isset($settings['clean']) ? $settings['clean'] : 1;
  if ($clean) {
    $values = array_filter($values, function($value) {
      if (is_array($value)) {
        return array_filter($value) !== array();
      }

      return (string) $value !== '';
    });
  }

  $values['value'] = serialize($values);
  form_set_value($element, $values, $form_state);
}
